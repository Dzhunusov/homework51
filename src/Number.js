import React from 'react';

const Number = props => {
  return (
      <div className= "number-item">
        <p>{props.number}</p>
      </div>
  )
}

export default Number;